<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m190815_170613_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'type' => $this->string()->defaultValue(\app\models\Settings::TYPE_TEXT)->comment('Тип'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);

        $this->insert('settings', [
            'key' => 'commission_corporation_card',
            'value' => '0',
            'label' => 'Комиссия за корпаративную карту (%)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_cashless',
            'value' => '0',
            'label' => 'Комиссия за безналичный расчет (%)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_unistream',
            'value' => '0',
            'label' => 'Комиссия за юнистрим (%)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_visa_and_mastercard',
            'value' => '0',
            'label' => 'Комиссия за визу и мастеркард (%)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_union_pay',
            'value' => '0',
            'label' => 'Комиссия за юнион пей (%)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_swift',
            'value' => '0',
            'label' => 'Комиссия за SWIFT (%)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_crypt',
            'value' => '0',
            'label' => 'Комиссия за криптовалюту (%)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_documents',
            'value' => '0',
            'label' => 'Комиссия за документы (%)',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
