<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ref_agent`.
 */
class m190902_163022_create_ref_agent_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ref_agent', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'agent_id' => $this->integer()->comment('Приглашенный пользователь'),
            'email' => $this->string()->comment('Email пользователя'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-ref_agent-user_id',
            'ref_agent',
            'user_id'
        );

        $this->addForeignKey(
            'fk-ref_agent-user_id',
            'ref_agent',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-ref_agent-agent_id',
            'ref_agent',
            'agent_id'
        );

        $this->addForeignKey(
            'fk-ref_agent-agent_id',
            'ref_agent',
            'agent_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-ref_agent-agent_id',
            'ref_agent'
        );

        $this->dropIndex(
            'idx-ref_agent-agent_id',
            'ref_agent'
        );

        $this->dropForeignKey(
            'fk-ref_agent-user_id',
            'ref_agent'
        );

        $this->dropIndex(
            'idx-ref_agent-user_id',
            'ref_agent'
        );

        $this->dropTable('ref_agent');
    }
}
