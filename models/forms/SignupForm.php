<?php
namespace app\models\forms;

use app\models\RefAgent;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $userId;
    public $email;
    public $password;
    public $repeatPassword;
    public $ref_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'repeatPassword'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            [['ref_id'], 'integer'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'targetAttribute' => 'login', 'message' => 'Этот email уже зарегистрирован'],
            ['password', 'string', 'min' => 6],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль',
            'ref_id' => 'Реферальная ссылка'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->login = $this->email;
        $user->setPassword($this->password);
        $user->password = $this->password;
        $user->role = User::ROLE_USER;
        $user->save(false);

        if($this->ref_id){
            $ref = RefAgent::findOne($this->ref_id);
            if($ref){
                $ref->agent_id = $user->id;
                $ref->save(false);
            }
        }

        return $user;
    }

//    /**
//     * update user.
//     *
//     * @param User $user
//     * @return User|null the saved model or null if saving fails
//     */
//    public function update($user)
//    {
//        if (!$this->validate()) {
//            return null;
//        }
//
//        $user->name = $this->name;
//        $user->surname = $this->surname;
//        $user->patronymic = $this->patronymic;
//        $user->phone = $this->phone;
//        $user->category = $this->category;
//        $user->department = $this->department;
//        $user->email = $this->email;
//        $user->setPassword($this->password);
//        $user->password = $this->password;
//        // $user->generateAuthKey();
//
//        return $user->update();
//    }
}
