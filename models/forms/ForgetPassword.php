<?php

namespace app\models\forms;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Class ForgetPassword
 * @package app\models\forms
 */
class ForgetPassword extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * @return bool
     */
    public function sendNewPassword()
    {
        if($this->validate()){

            $newPassword = Yii::$app->security->generateRandomString(10);

            /** @var User $user */
            $user = User::find()->where(['login' => $this->email])->one();

            if($user){
                $user->setPassword($newPassword);
                $user->save(false);
            }

            Yii::$app->mailer->compose()
                ->setFrom('money.notification.service@mail.ru')
                ->setTo($user->login)
                ->setSubject('Востанавление пароля')
                ->setHtmlBody('Ваш новый пароль: '.$newPassword)
                ->send();

            return true;
        }

        return false;
    }
}