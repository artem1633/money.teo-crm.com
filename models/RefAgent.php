<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ref_agent".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $agent_id Приглашенный пользователь
 * @property string $email Email пользователя
 * @property string $created_at
 *
 * @property integer $status
 *
 * @property User $agent
 * @property User $user
 */
class RefAgent extends \yii\db\ActiveRecord
{
    const STATUS_SENT = 0;
    const STATUS_ONLINE = 1;
    const STATUS_OFFLINE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_agent';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => null,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['user_id', 'agent_id'], 'integer'],
            [['created_at'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['agent_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'agent_id' => 'Приглашенный пользователь',
            'email' => 'Email пользователя',
            'created_at' => 'Дата и время создания',
        ];
    }

    /**
     * Возвращает статус агента
     * @return integer
     */
    public function getStatus()
    {
        if($this->user_id == null){
            return self::STATUS_SENT;
        }

        $user = $this->user;

        if($user->isOnline()){
            return self::STATUS_ONLINE;
        } else {
            return self::STATUS_OFFLINE;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(User::className(), ['id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
