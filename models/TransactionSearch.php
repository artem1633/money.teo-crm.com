<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transaction;

/**
 * TransactionSearch represents the model behind the search form about `app\models\Transaction`.
 */
class TransactionSearch extends Transaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'manager_id', 'type', 'method', 'currency', 'purpose_id', 'is_require_documents', 'is_documents_nds', 'document_purpose_id', 'status'], 'integer'],
            [['amount', 'summary_price_amount'], 'number'],
            [['inn', 'kpp', 'fio', 'phone', 'town', 'card_number', 'cardholders_name', 'file_requisites', 'wallet_number', 'manager_accepted_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'manager_id' => $this->manager_id,
            'type' => $this->type,
            'method' => $this->method,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'purpose_id' => $this->purpose_id,
            'is_require_documents' => $this->is_require_documents,
            'is_documents_nds' => $this->is_documents_nds,
            'document_purpose_id' => $this->document_purpose_id,
            'summary_price_amount' => $this->summary_price_amount,
            'status' => $this->status,
            'manager_accepted_at' => $this->manager_accepted_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'town', $this->town])
            ->andFilterWhere(['like', 'card_number', $this->card_number])
            ->andFilterWhere(['like', 'cardholders_name', $this->cardholders_name])
            ->andFilterWhere(['like', 'file_requisites', $this->file_requisites])
            ->andFilterWhere(['like', 'wallet_number', $this->wallet_number]);

        return $dataProvider;
    }
}
