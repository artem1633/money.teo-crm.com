<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RefAgent */
?>
<div class="ref-agent-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'agent_id',
            'email:email',
            'created_at',
        ],
    ]) ?>

</div>
