<?php

use yii\helpers\Url;

?>

<div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Пополнение', 'icon' => 'fa  fa-plus', 'url' => ['/transaction-in'],],
                    ['label' => 'Вывод', 'icon' => 'fa  fa-minus', 'url' => ['/transaction-out'],],
                    ['label' => 'Настройки', 'icon' => 'fa  fa-cog', 'url' => ['/settings'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Агенты', 'icon' => 'fa  fa-users', 'url' => ['/ref-agent/index'],],
                    ['label' => 'Тикеты', 'icon' => 'fa  fa-pencil', 'url' => ['/ticket'],],
                    ['label' => 'Новости', 'icon' => 'fa  fa-th', 'url' => ['/news'],],
                    ['label' => 'ЧАВО', 'icon' => 'fa  fa-th', 'url' => ['/faq'],],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
