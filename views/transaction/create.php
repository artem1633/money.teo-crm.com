<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
/* @var $type int */

$this->title = 'Создание заявки';

?>
<div class="transaction-create">
    <?php if($type == 0): ?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    <?php elseif($type == 1): ?>
        <?= $this->render('_form_out', [
            'model' => $model,
        ]) ?>
    <?php endif; ?>



</div>
