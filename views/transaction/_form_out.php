<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
/* @var $settings \app\components\Settings */

$settings = Yii::$app->settings;

$model->type = \app\models\Transaction::TYPE_OUT;

?>

    <div class="transaction-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Создание</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model, 'amount')->textInput() ?>
                    </div>

                    <div class="hidden">
                        <?= $form->field($model, 'type')->hiddenInput() ?>
                    </div>

                    <div class="col-md-9">
                        <?= $form->field($model, 'method')->widget(\app\widgets\ButtonsSelect::className(), [
                            'data' => [
                                \app\models\Transaction::METHOD_UNISTREAM => '<img src="/img/unistream.png" style="height: 17px;"> Денежный перевод Юнистрим',
                                \app\models\Transaction::METHOD_VISA_AND_MASTER_CARD => '<img src="/img/visa-and-mastercard.jpg" style="height: 17px;"> Виза и Мастеркард',
                                \app\models\Transaction::METHOD_UNIONPAY => '<img src="/img/unionpay.png" style="height: 17px;"> На любую китайскую карту юнион пей',
                                \app\models\Transaction::METHOD_SWIFT => '<img src="/img/swift.png" style="height: 17px;"> SWIFT перевод',
                                \app\models\Transaction::METHOD_CRYPT => '<img src="/img/bitcoin.png" style="height: 17px;"> Криптовалюта',
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label">Сумма к оплате</label>
                            <input id="payment-sum" class="form-control" type="text" disabled>
                        </div>
                    </div>
                </div>

                <div class="unistream-wrapper" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'fio')->textInput() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'phone')->textInput() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'town')->textInput() ?>
                        </div>
                    </div>
                </div>

                <div class="visa-mastercard-wrapper unionpay-wrapper" style="display: none;">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'card_number')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'cardholders_name')->textInput() ?>
                        </div>
                    </div>
                </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'currency')->widget(\app\widgets\ButtonsSelect::className(), [
                                'data' => [
                                    \app\models\Transaction::CURRENCY_BTC => 'BTC',
                                    \app\models\Transaction::CURRENCY_USDT => 'USDT',
                                ],
                            ]) ?>
                        </div>
                    </div>

                <div class="btc-wrapper" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'wallet_number')->textInput() ?>
                        </div>
                    </div>
                </div>

                <div class="swift-wrapper" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'file')->fileInput() ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Отправить заявку' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php

$script = <<< JS

    var comissionUnistream = {$settings->commission_unistream};
    var comissionVisaAndMastercard = {$settings->commission_visa_and_mastercard};
    var comissionUnionPay= {$settings->commission_union_pay};
    var comissionSwift = {$settings->commission_swift};
    var comissionCrypt = {$settings->commission_crypt};
    var comissionDocuments = {$settings->commission_documents};
    
    var currentPercent = comissionUnistream;

    $('[name="Transaction[method]"]').change(function(){
        var value = $(this).val();
        
        if(value == 2){
            currentPercent = comissionUnistream;
            $('.unistream-wrapper').slideDown();
            $('.unionpay-wrapper, .visa-mastercard-wrapper, .swift-wrapper, .btc-wrapper').slideUp();
        } else if(value == 3) {
            currentPercent = comissionVisaAndMastercard;
            $('.visa-mastercard-wrapper').slideDown();
            $('.unistream-wrapper, .swift-wrapper, .btc-wrapper').slideUp();
        } else if(value == 4) {
            currentPercent = comissionUnionPay;
            $('.unionpay-wrapper').slideDown();
            $('.unistream-wrapper, .swift-wrapper, .btc-wrapper').slideUp();
        } else if(value == 5) {
            currentPercent = comissionSwift;
            $('.swift-wrapper').slideDown();
            $('.unistream-wrapper, .visa-mastercard-wrapper, .btc-wrapper').slideUp();
        } else if(value == 6) {
            currentPercent = comissionCrypt;
            $('.btc-wrapper').slideDown();
            $('.unistream-wrapper, .visa-mastercard-wrapper, .swift-wrapper').slideUp();
        }
        
        if(value == 1){
            $('#method-cashless-wrapper').slideDown();
        } else {
            $('#method-cashless-wrapper').slideUp();
        }
        
        var amount = $('#transaction-amount').val();
        
        var sum = amount - Math.round(amount / 100 * currentPercent);
        
        $('#payment-sum').val(sum);
    });

    $('[name="Transaction[is_require_documents]"]').change(function(){ 
        if(value == 1){
            $('#require-document-wrapper').slideDown();
        } else {
            $('#require-document-wrapper').slideUp();
        }
    });
    
    $('#transaction-amount').keyup(function() {
        var value = $(this).val();
        
        var sum = value - Math.round(value / 100 * currentPercent);
        
        $('#payment-sum').val(sum);
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>