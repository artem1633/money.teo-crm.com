<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TransactionPurpose */
?>
<div class="transaction-purpose-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
