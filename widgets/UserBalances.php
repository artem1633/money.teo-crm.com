<?php

namespace app\widgets;

use app\models\User;
use yii\base\InvalidConfigException;
use yii\base\Widget;

/**
 * Class UserBalances
 * @package app\widgets
 */
class UserBalances extends Widget
{
    /**
     * @var User
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->user == null){
            throw new InvalidConfigException('user must be required');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        $html = '';
        $html .= "<p>{$this->user->balance_rub} <i class='fa fa-rub'></i></p>";
        $html .= "<p>{$this->user->balance_usd} <i class='fa fa-usd'></i></p>";
        $html .= "<p>{$this->user->balance_eur} <i class='fa fa-eur'></i></p>";
        $html .= "<p>{$this->user->balance_cny} <i class='fa fa-cny'></i></p>";

        return $html;
    }
}